# PQCrypto 2020 website

Built with [zola](https://github.com/getzola/zola).

```sh
$ zola build
$ rsync -r public/ $WEBSITE_LOCATION
```
