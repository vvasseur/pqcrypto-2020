+++
title = "Home"
description = "The Eleventh International Conference on Post-Quantum Cryptography"
weight = 0
url = ""
template = "page.html"
sort_by = "weight"
+++
# The Eleventh International Conference on Post-Quantum Cryptography
## Paris, France, ~~April 15-17, 2020~~ September 21-23, 2020

# Introduction
The aim of PQCrypto is to serve as a forum for researchers to present results and exchange ideas on the topic of cryptography in an era with large-scale quantum computers.

After ten successful PQCrypto conferences
([2006](https://postquantum.cr.yp.to/) in Leuven,
[2008](https://web.archive.org/web/20160818142141/https://math.uc.edu/~aac/pqcrypto2008/) in Cincinnati,
[2010](https://web.archive.org/web/20101008051348/http://pqc2010.cased.de/) in Darmstadt,
[2011](http://pq.crypto.tw/pqc11/) in Taipei,
[2013](http://pqcrypto2013.xlim.fr/) in Limoges,
[2014](https://web.archive.org/web/20190502203201/http://pqcrypto2014.uwaterloo.ca/) in Waterloo,
[2016](https://pqcrypto2016.jp/) in Fukuoka,
[2017](https://2017.pqcrypto.org/conference/) in Utrecht,
[2018](http://www.math.fau.edu/pqcrypto2018/) in Fort Lauderdale,
and [2019](https://pqcrypto2019.org/) in Chongqing),
PQCrypto 2020 will take place September 21-23, 2020 in Paris.

# The conference was virtual

Recordings and live sessions are freely available on the [PQCrypto 2020 YouTube channel](https://www.youtube.com/channel/UC7zz9AQLYzoMNsmotzC__1Q).

* [Watch the replay of the first day](https://youtu.be/HfmvNenGyok)
* [Watch the replay of the second day](https://youtu.be/lxx2xE_k7Wc)
* [Watch the replay of the third day](https://youtu.be/CBGX1OMzN1o)

Direct links to the individual presentations are given in the [program section](@/program.md).

# Contact
[pqcrypto2020-orga@inria.fr](mailto:pqcrypto2020-orga@inria.fr)
