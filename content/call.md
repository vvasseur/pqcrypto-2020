+++
title = "Call"
description = "Call for paper"
weight = 80
url = "call"
template = "page.html"
+++
# Important dates
- Initial submission deadline: November 26, 2019 (23:59 UTC-11:00).
    * All Papers must be submitted by the initial submission deadline. Authors
      may continue to revise their submissions until the final submission
      deadline.
- Final submission deadline: December 3, 2019 (23:59 UTC-11:00).
    * Between the deadlines, PC members will have access to the title and
      abstract of the papers, but not to the PDF files. The abstract should
      summarize the contributions of the paper at a level appropriate for a
      non-specialist reader.
- Notification about acceptance: January 20, 2020.
- Final version of accepted papers: February 3, 2020.
- Conference: ~~April 15-17, 2020~~ September 21-23, 2020.

# Submit paper
Please submit your paper to [EasyChair](https://easychair.org/conferences/?conf=pqcrypto2020).

# [Call for papers](pqc20-cfp.pdf)
Original research papers on all technical aspects of cryptographic research related to post-quantum cryptography are solicited. The topics include (but are not restricted to):
1. Cryptosystems that have the potential to be safe against quantum computers such as: code-based, hash-based, isogeny-based, lattice-based, and multivariate constructions.
2. Cryptanalysis of post-quantum systems, and quantum cryptanalysis.
3. Implementations of, and side-channel attacks on, post-quantum cryptosystems.
4. Security models for the post-quantum era.

# Instructions for authors
* Papers submitted by the initial submission deadline may be in draft form but must include a title and an abstract.
* Accepted papers are planned to be published in Springer's LNCS series.
* Submissions must not exceed 12 pages, excluding references and appendices in a single column format in 10pt fonts using the default llncs class without adjustments.
* Final version of accepted papers will be limited to 20 pages.
* Submissions must not substantially duplicate work that any of the authors has published in a journal or a conference/workshop with proceedings, or has submitted/is planning to submit before the authors notification deadline to a journal or other conferences/workshops that have proceedings.
* Submissions should begin with a title, the authors' names and affiliations, a short abstract, and a list of keywords.

Submissions ignoring these guidelines may be rejected without further consideration.
