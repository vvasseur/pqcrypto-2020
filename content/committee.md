+++
title = "Committee"
description = "Composition of the committee"
weight = 50
url = "committee"
template = "page.html"
+++
# General Chairs
* Antoine Joux, Sorbonne Université (FR)
* Nicolas Sendrier, Inria (FR)

# Organization Committee
* Axelle Balekdjian
* Chantal Girodon
* Matthieu Lequesne
* Lucile Moreno
* Hélène Robak
* Valentin Vasseur

# Program Chairs
* Jintai Ding, University of Cincinnati (US)
* Jean-Pierre Tillich, Inria (FR)

# Program Committee
* Yoshinori Aono, NICT (JP)
* Jean-Philippe Aumasson, Teserakt AG (CH)
* Reza Azarderakhsh, Florida Atlantic University & PQSecure Technologies (US)
* Magali Bardet, University of Rouen (FR)
* Daniel J. Bernstein, University of Illinois at Chicago (US) & Ruhr University Bochum (DE)
* Olivier Blazy, University of Limoges (FR)
* André Chailloux, Inria (FR)
* Chen-Mou Cheng, Osaka University & Kanazawa University (JP)
* Jung Hee Cheon, Seoul National University (KR)
* Tung Chou, Osaka University (JP) & Academia Sinica (TW)
* Dung Duong, University of Wollongong (AU)
* Scott Fluhrer, Cisco Systems (US)
* Philippe Gaborit, Limoges University (FR)
* Tommaso Gagliardoni, Kudelski Security (CH)
* Steven Galbraith, University of Auckland (NZ)
* Xiao-Shan Gao, Chinese Academy of Sciences, (CN)
* Tim Güneysu, Ruhr-University of Bochum & DFKI (DE)
* David Jao, University of Waterloo & evolutionQ (CA)
* Jiwu Jing, University of Chinese Academy of Sciences (CN)
* Thomas Johansson, Lund University (SE)
* Antoine Joux, Sorbonne University (FR)
* Kwangjo Kim, KAIST (KR)
* Elena Kirshanova, I.Kant Baltic Federal University (RU)
* Yi-Kai Liu, NIST & University of Maryland (US)
* Prabhat Mishra, University of Forida (US)
* Michele Mosca, Waterloo University & Perimeter Inst. (CA)
* María Naya-Plasencia, Inria (FR)
* Khoa Nguyen, Nanyang Technological University, (SG)
* Ruben Niederhagen, Fraunhofer SIT (DE)
* Ray Perlner, NIST (US)
* Christophe Petit, University of Birmingham, (UK)
* Rachel Player, University of London (GB)
* Thomas Pöppelmann, Infineon Technologies (DE)
* Thomas Prest, PQShield (UK)
* Nicolas Sendrier, Inria (FR)
* Junji Shikata, Yokohama National University (JP)
* Daniel Smith-Tone, NIST & University of Louisville (US)
* Rainer Steinwandt, Florida Atlantic University, (US)
* Damien Stehlé, ENS de Lyon (FR)
* Tsuyoshi Takagi, University of Tokyo (JP)
* Routo Terada, University of São Paulo (BR)
* Serge Vaudenay, EPFL (CH)
* Keita Xagawa, NTT Secure Platform Laboratories (JP)
* Bo-Yin Yang, Academia Sinica (TW)
* Zhenfeng Zhang, Institute of Software, Chinese Academy of Sciences (CN)
