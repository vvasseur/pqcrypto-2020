+++
title = "Accepted papers"
description = "List of accepted papers"
weight = 10
url = "papers"
template = "page.html"
+++
* {{paper(title="Defeating «NewHope» with a Single Trace", authors="Dorian Amiet, Andreas Curiger, Lukas Leuenberger and Paul Zbinden") }}
* {{paper(title="Combinatorial Rank Attacks Against the Rectangular Simple Matrix Encryption Scheme", authors="Daniel Apon, Dustin Moody, Ray Perlner, Daniel Smith-Tone and Javier Verbel") }}
* {{paper(title="LegRoast: Efficient post-quantum signatures from the Legendre PRF", authors="Ward Beullens and Cyprien Delpech de Saint Guilhem") }}
* {{paper(title="Decryption failure is more likely after success", authors="Nina Bindel and John M. Schanck") }}
* {{paper(title="Compact Privacy Protocols from Post-Quantum and Timed Classical Assumptions", authors="Jonathan Bootle, Anja Lehmann, Vadim Lyubashevsky and Gregor Seiler") }}
* {{paper(title="Efficient Post-Quantum SNARKs for RSIS and RLWE and their Applications to Privacy", authors="Cecilia Boschini, Jan Camenisch, Nicholas Spooner and Max Ovsiankin") }}
* {{paper(title="CSIDH on the surface", authors="Wouter Castryck and Thomas Decru") }}
* {{paper(title="The supersingular isogeny problem in genus 2 and beyond", authors="Craig Costello and Benjamin Smith") }}
* {{paper(title="Sashimi: Cutting up CSI-FiSh secret keys to produce an actively secure distributed signing protocol", authors="Daniele Cozzo and Nigel Smart") }}
* {{paper(title="Many a Mickle Makes a Muckle: A Framework for Provably Quantum-Secure Hybrid Key Exchange", authors="Benjamin Dowling, Torben Brandt Hansen and Kenneth G. Paterson") }}
* {{paper(title="QC-MDPC decoders with several shades of gray", authors="Nir Drucker, Shay Gueron and Dusan Kostic") }}
* {{paper(title="A Note on the Instantiability of the Quantum Random Oracle", authors="Edward Eaton and Fang Song") }}
* {{paper(title="A Structural Attack on Block-Anti-Circulant UOV at SAC 2019", authors="Hiroki Furue, Koha Kinjo, Yasuhiko Ikematsu, Yacheng Wang and Tsuyoshi Takagi") }}
* {{paper(title="Collapseability of Tree Hashes", authors="Aldo Gunsing and Bart Mennink") }}
* {{paper(title="Improved quantum circuits for elliptic curve discrete logarithms", authors="Thomas Häner, Samuel Jaques, Michael Naehrig, Martin Roetteler and Mathias Soeken") }}
* {{paper(title="The Power of Few Qubits and Collisions -- Subset Sum below Grover's Bound", authors="Alexander Helm and Alexander May") }}
* {{paper(title="On quantum distinguishers for Type-3 Generalized Feistel network based on separability", authors="Samir Hodžić, Lars Ramkilde Knudsen and Andreas Brasen Kidmose") }}
* {{paper(title="Isochronous Gaussian Sampling: From Inception to Implementation", authors="James Howe, Thomas Prest, Thomas Ricosset and Mélissa Rossi") }}
* {{paper(title="Encryption Schemes using Random Oracles: from Classical to Post-Quantum Security", authors="Juliane Krämer and Patrick Struck") }}
* {{paper(title="Benchmarking Post-Quantum Cryptography in TLS", authors="Christian Paquin, Douglas Stebila and Goutam Tamvada") }}
* {{paper(title="Efficient Key Generation for Rainbow", authors="Albrecht Petzoldt") }}
* {{paper(title="Randomized Decoding of Gabidulin Codes Beyond the Unique Decoding Radius", authors="Julian Renner, Thomas Jerkovits, Hannes Bartz, Sven Puchinger, Pierre Loidreau and Antonia Wachter-Zeh") }}
* {{paper(title="Generalization of Isomorphism of Polynomials with Two Secrets and Its Application to Public Key Encryption", authors="Bagus Santoso") }}
* {{paper(title="About Low DFR for QC-MDPC Decoding", authors="Nicolas Sendrier and Valentin Vasseur") }}
* {{paper(title="A Rank Attack Against Extension Field Cancellation", authors="Daniel Smith-Tone and Javier Verbel") }}
* {{paper(title="Practical Cryptanalysis of k-ary C\*", authors="Daniel Smith-Tone") }}
* {{paper(title="Short Zero-Knowledge Proof of Knowledge for Lattice-Based Commitment", authors="Yang Tao, Xi Wang and Rui Zhang") }}
* {{paper(title="Multivariate Encryption Schemes Based on Polynomial Equations over Real Numbers", authors="Takanori Yasuda, Yacheng Wang and Tsuyoshi Takagi") }}
* {{paper(title="COSAC: COmpact and Scalable Arbitrary-Centered Discrete Gaussian Sampling over Integers", authors="Raymond K. Zhao, Ron Steinfeld and Amin Sakzad") }}
