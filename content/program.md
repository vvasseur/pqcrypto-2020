+++
title = "Program"
description = "Conference program"
weight = 20
url = "program"
template = "page.html"
+++
# Invited speakers
- [Benjamin Smith](https://www.lix.polytechnique.fr/~smith/) on isogenies
- [Frank Wilhelm-Mauch](https://www.uni-saarland.de/fachrichtung/physik/professuren/theoretische-physik/wilhelm-mauch.html) on the state of play and the roadmap for quantum computing
- NIST on the [Post-Quantum Cryptography Standardization](https://csrc.nist.gov/Projects/post-quantum-cryptography/Post-Quantum-Cryptography-Standardization)

# Time table
All times are given in Paris local time (UTC+02:00).

## Monday, September 21 (UTC+02:00)
{{ day_begin() }}
{{ slot_begin(time_begin="13:55", time_end="14:00", invited=0, live="https://youtu.be/HfmvNenGyok", theme="Welcome remarks") }}


{{ slot_end() }}

{{ slot_begin(time_begin="14:00", time_end="15:00", invited="1", chair="Christophe Petit", live="https://youtu.be/HfmvNenGyok", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-11", theme="1.1 - Invited talk") }}

* {{paper(title="Isogenies: what now, and what next?", authors="Benjamin Smith") }}

{{ slot_end() }}

{{ slot_begin(time_begin="15:10", time_end="15:50", invited=0, chair="Reza Azarderakhsh, Christophe Petit", live="https://youtu.be/HfmvNenGyok", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-12", theme="1.2 - Isogeny-based and Number Theoretic-based Cryptography") }}

* {{paper(title="CSIDH on the surface", authors="Wouter Castryck and Thomas Decru", preprint="https://eprint.iacr.org/2019/1404.pdf", video="https://youtu.be/hXyE3qg09gA") }}
* {{paper(title="Sashimi: Cutting up CSI-FiSh secret keys to produce an actively secure distributed signing protocol", authors="Daniele Cozzo and Nigel Smart", preprint="https://eprint.iacr.org/2019/1360.pdf", video="https://youtu.be/Xb_iXgsiCGk") }}
* {{paper(title="The supersingular isogeny problem in genus 2 and beyond", authors="Craig Costello and Benjamin Smith", preprint="https://arxiv.org/pdf/1912.00701.pdf", video="https://youtu.be/0B08wdbUlgg") }}
* {{paper(title="LegRoast: Efficient post-quantum signatures from the Legendre PRF", authors="Ward Beullens and Cyprien Delpech de Saint Guilhem", preprint="https://eprint.iacr.org/2020/128.pdf", video="https://youtu.be/rGPIuljfdaI") }}

{{ slot_end() }}

{{ slot_begin(time_begin="16:00", time_end="17:00", invited=0, chair="Magali Bardet, Rachel Player", live="https://youtu.be/HfmvNenGyok", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-13", theme="1.3 - Multivariate Cryptography") }}

* {{paper(title="Multivariate Encryption Schemes Based on Polynomial Equations over Real Numbers", authors="Takanori Yasuda, Yacheng Wang and Tsuyoshi Takagi", video="https://youtu.be/QCfRACtOJ7Y") }}
* {{paper(title="A Structural Attack on Block-Anti-Circulant UOV at SAC 2019", authors="Hiroki Furue, Koha Kinjo, Yasuhiko Ikematsu, Yacheng Wang and Tsuyoshi Takagi", video="https://youtu.be/10ybIYlWnTU") }}
* {{paper(title="Combinatorial Rank Attacks Against the Rectangular Simple Matrix Encryption Scheme", authors="Daniel Apon, Dustin Moody, Ray Perlner, Daniel Smith-Tone and Javier Verbel", video="https://youtu.be/UJIo0rKrpko") }}
* {{paper(title="Practical Cryptanalysis of k-ary C\*", authors="Daniel Smith-Tone", preprint="https://eprint.iacr.org/2019/841.pdf", video="https://youtu.be/gIp6m1Gqoyk") }}
* {{paper(title="A Rank Attack Against Extension Field Cancellation", authors="Daniel Smith-Tone and Javier Verbel", video="https://youtu.be/CSw8hl8abiQ") }}
* {{paper(title="Generalization of Isomorphism of Polynomials with Two Secrets and Its Application to Public Key Encryption", authors="Bagus Santoso", video="https://youtu.be/YWAjqLzxOvY") }}

{{ slot_end() }}

{{ day_end() }}

## Tuesday, September 22 (UTC+02:00)
{{ day_begin() }}
{{ slot_begin(time_begin="14:00", time_end="15:00", invited="1", chair="Tommaso Gagliardoni", live="https://youtu.be/lxx2xE_k7Wc", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-21", theme="2.1 - Invited talk") }}

* {{paper(title="Quantum computers - state of play and roadmap", authors="Frank Wilhelm-Mauch") }}

{{ slot_end() }}

{{ slot_begin(time_begin="15:10", time_end="15:40", invited=0, chair="Tommaso Gagliardoni, Rainer Steinwandt", live="https://youtu.be/lxx2xE_k7Wc", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-22", theme="2.2 - Quantum Algorithms") }}

* {{paper(title="The Power of Few Qubits and Collisions -- Subset Sum below Grover's Bound", authors="Alexander Helm and Alexander May", preprint="http://www.cits.ruhr-uni-bochum.de/imperia/md/content/may/paper/pqc20_subset_sum_below_grover.pdf", video="https://youtu.be/Og7Gl7PzLds") }}
* {{paper(title="On quantum distinguishers for Type-3 Generalized Feistel network based on separability", authors="Samir Hodžić, Lars Ramkilde Knudsen and Andreas Brasen Kidmose", video="https://youtu.be/miCMnUj959M") }}
* {{paper(title="Improved quantum circuits for elliptic curve discrete logarithms", authors="Thomas Häner, Samuel Jaques, Michael Naehrig, Martin Roetteler and Mathias Soeken", preprint="https://arxiv.org/pdf/2001.09580", video="https://youtu.be/aoFq0VskfwQ") }}

{{ slot_end() }}

{{ slot_begin(time_begin="15:50", time_end="16:20", invited=0, chair="Ruben Niederhagen, Rainer Steinwandt", live="https://youtu.be/lxx2xE_k7Wc", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-23", theme="2.3 - Implementation") }}

* {{paper(title="Isochronous Gaussian Sampling: From Inception to Implementation", authors="James Howe, Thomas Prest, Thomas Ricosset and Mélissa Rossi", video="https://youtu.be/KqCnJApHC3w") }}
* {{paper(title="Efficient Key Generation for Rainbow", authors="Albrecht Petzoldt", video="https://youtu.be/iFo-mx8isCc") }}
* {{paper(title="Benchmarking Post-Quantum Cryptography in TLS", authors="Christian Paquin, Douglas Stebila and Goutam Tamvada", preprint="https://s3.amazonaws.com/files.douglas.stebila.ca/files/research/papers/PQCrypto-PaqSteTam20.pdf", video="https://youtu.be/oAWoTm5is_0") }}

{{ slot_end() }}

{{ slot_begin(time_begin="16:30", time_end="17:10", invited=0, chair="Olivier Blazy, Thomas Prest", live="https://youtu.be/lxx2xE_k7Wc", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-24", theme="2.4 - Security Proofs") }}

* {{paper(title="Many a Mickle Makes a Muckle: A Framework for Provably Quantum-Secure Hybrid Key Exchange", authors="Benjamin Dowling, Torben Brandt Hansen and Kenneth G. Paterson", preprint="https://eprint.iacr.org/2020/099.pdf", video="https://youtu.be/r_9AZI0jxYY") }}
* {{paper(title="Collapseability of Tree Hashes", authors="Aldo Gunsing and Bart Mennink", preprint="https://www.cs.ru.nl/~bmennink/pubs/20pqc.pdf", video="https://youtu.be/cC23W2Dr-1A") }}
* {{paper(title="Encryption Schemes using Random Oracles: from Classical to Post-Quantum Security", authors="Juliane Krämer and Patrick Struck", preprint="https://eprint.iacr.org/2020/129.pdf", video="https://youtu.be/K_LYxhwGbpg") }}
* {{paper(title="A Note on the Instantiability of the Quantum Random Oracle", authors="Edward Eaton and Fang Song", preprint="https://eprint.iacr.org/2019/1466.pdf", video="https://youtu.be/dIpL8gu_39c") }}

{{ slot_end() }}

{{ day_end() }}

## Wednesday, September 23 (UTC+02:00)
{{ day_begin() }}
{{ slot_begin(time_begin="13:55", time_end="14:00", invited=0, live="https://youtu.be/CBGX1OMzN1o", theme="Closing remarks") }}


{{ slot_end() }}

{{ slot_begin(time_begin="14:00", time_end="15:00", invited="1", chair="Damien Stehlé", live="https://youtu.be/CBGX1OMzN1o", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-31", theme="3.1 - Invited talk") }}

* {{paper(title="NIST PQC Standardization Update: Round 2 and beyond", authors="Dustin Moody") }}

{{ slot_end() }}

{{ slot_begin(time_begin="15:10", time_end="16:10", invited=0, chair="Elena Kirshanova, Damien Stehlé", live="https://youtu.be/CBGX1OMzN1o", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-32", theme="3.2 - Lattice-based Cryptography") }}

* {{paper(title="COSAC: COmpact and Scalable Arbitrary-Centered Discrete Gaussian Sampling over Integers", authors="Raymond K. Zhao, Ron Steinfeld and Amin Sakzad", preprint="https://eprint.iacr.org/2019/1011.pdf", video="https://youtu.be/t69Ip7ztNcw") }}
* {{paper(title="Short Zero-Knowledge Proof of Knowledge for Lattice-Based Commitment", authors="Yang Tao, Xi Wang and Rui Zhang", video="https://youtu.be/oGaNbeoKRxU") }}
* {{paper(title="Compact Privacy Protocols from Post-Quantum and Timed Classical Assumptions", authors="Jonathan Bootle, Anja Lehmann, Vadim Lyubashevsky and Gregor Seiler", video="https://youtu.be/0f1AAVRIS54") }}
* {{paper(title="Defeating «NewHope» with a Single Trace", authors="Dorian Amiet, Andreas Curiger, Lukas Leuenberger and Paul Zbinden", preprint="https://www.imes.hsr.ch/fileadmin/user_upload/imes.hsr.ch/Publikationen/200410_Defeating_NewHope_with_a_Single_Trace.pdf", video="https://youtu.be/6jg21ffleqA") }}
* {{paper(title="Decryption failure is more likely after success", authors="Nina Bindel and John M. Schanck", preprint="https://eprint.iacr.org/2019/1392.pdf", video="https://youtu.be/MydOujbq8r0") }}
* {{paper(title="Efficient Post-Quantum SNARKs for RSIS and RLWE and their Applications to Privacy", authors="Cecilia Boschini, Jan Camenisch, Nicholas Spooner and Max Ovsiankin", video="https://youtu.be/05nET87n460") }}

{{ slot_end() }}

{{ slot_begin(time_begin="16:20", time_end="16:50", invited=0, chair="Philippe Gaborit, Jean-Pierre Tillich", live="https://youtu.be/CBGX1OMzN1o", chat="https://mattermost.pqcrypto.fr/pqcrypto-2020/channels/session-33", theme="3.3 - Code-based Cryptography") }}

* {{paper(title="QC-MDPC decoders with several shades of gray", authors="Nir Drucker, Shay Gueron and Dusan Kostic", preprint="https://eprint.iacr.org/2019/1423.pdf", video="https://youtu.be/1mZ7h3ljHyk") }}
* {{paper(title="About Low DFR for QC-MDPC Decoding", authors="Nicolas Sendrier and Valentin Vasseur", preprint="https://eprint.iacr.org/2019/1434.pdf", video="https://youtu.be/QYlAy1sRhpA") }}
* {{paper(title="Randomized Decoding of Gabidulin Codes Beyond the Unique Decoding Radius", authors="Julian Renner, Thomas Jerkovits, Hannes Bartz, Sven Puchinger, Pierre Loidreau and Antonia Wachter-Zeh", preprint="https://arxiv.org/pdf/1911.13193", video="https://youtu.be/Bs-lK8CCz5E") }}

{{ slot_end() }}

{{ day_end() }}

