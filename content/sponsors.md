+++
title = "Sponsors"
description = "Sponsors of the conference"
weight = 70
url = "sponsors"
template = "page.html"
+++
# Organization
PQCrypto 2020 is organized by Inria and Sorbonne Université with the support of the ERC Almacrypt (European Union's H2020 Program under grant agreement number ERC-669891).

# Main sponsors
- Amazon Web Services
- Cisco
- Worldline

# Sponsors
- Infineon
- PQShield
