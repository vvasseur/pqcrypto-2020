import sys
import yaml

yaml_data = {}
for yaml_file in sys.argv[1:]:
    with open(yaml_file) as f:
#        yaml_data.update(yaml.load(f, Loader=yaml.FullLoader))
        yaml_data.update(yaml.load(f))

for day in yaml_data["program"]:
    print("## {day} (UTC+02:00)".format(**day))
    print('{{ day_begin() }}')
    for slot in day["slots"]:
        string = '{{{{ slot_begin(time_begin="{begin}", time_end="{end}"'.format(**slot)
        if "invited" in slot:
            string += ', invited="{}"'.format(1 if slot["invited"] else 0)
        else:
            string += ', invited=0'
        if "chair" in slot:
            string += ', chair="{chair}"'.format(**slot)
        if "live" in slot:
            string += ', live="{live}"'.format(**slot)
        if "chat" in slot:
             string += ', chat="{chat}"'.format(**slot)
        if "title" in slot:
            string += ', theme="{title}"'.format(**slot)
        string += ') }}'
        print(string)
        print()
        if "papers" in slot:
            for paper in slot["papers"]:
                string = '* {{{{paper(title="{title}", authors="{authors}"'.format(**paper)
                if "preprint" in paper:
                    string += ', preprint="{preprint}"'.format(**paper)
                if "video" in paper:
                    string += ', video="{video}"'.format(**paper)
                string += ') }}'
                print(string)
        print()
        print('{{ slot_end() }}')
        print()
    print('{{ day_end() }}')
    print()
